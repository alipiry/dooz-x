# [DoozX](https://github.com/alipiry/dooz-x)

[DoozX](https://github.com/alipiry/dooz-x) is the Persian clone of XO game. It is written in 
[React Native](https://github.com/facebook/react-native) for both Android and iOS devices.
> [Website](https://alipiry.github.io/dooz-x)

## Status 

🚧 Under Development 🚧

## Installation

Clone the project:
```bash
  git clone git@github.com:alipiry/dooz-x.git
```
Change your current directory:
```bash
  cd dooz-x/
```
Simply run `yarn` to install all dependencies and dev dependencies:
```bash
  yarn
```

## Running

To run this project:
```bash
  yarn run start
```

## License 

DoozX is MIT-licensed.
